/**
 * @fileoverview
 * @enhanceable
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

goog.provide('proto.ro.tuc.ds2020.DownloadResponse');

goog.require('jspb.Message');
goog.require('jspb.BinaryReader');
goog.require('jspb.BinaryWriter');
goog.require('proto.ro.tuc.ds2020.MedPlan');


/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.ro.tuc.ds2020.DownloadResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.ro.tuc.ds2020.DownloadResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.ro.tuc.ds2020.DownloadResponse.displayName = 'proto.ro.tuc.ds2020.DownloadResponse';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.ro.tuc.ds2020.DownloadResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.ro.tuc.ds2020.DownloadResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.ro.tuc.ds2020.DownloadResponse} msg The msg instance to transform.
 * @return {!Object}
 */
proto.ro.tuc.ds2020.DownloadResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    plan: (f = msg.getPlan()) && proto.ro.tuc.ds2020.MedPlan.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.ro.tuc.ds2020.DownloadResponse}
 */
proto.ro.tuc.ds2020.DownloadResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.ro.tuc.ds2020.DownloadResponse;
  return proto.ro.tuc.ds2020.DownloadResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.ro.tuc.ds2020.DownloadResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.ro.tuc.ds2020.DownloadResponse}
 */
proto.ro.tuc.ds2020.DownloadResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.ro.tuc.ds2020.MedPlan;
      reader.readMessage(value,proto.ro.tuc.ds2020.MedPlan.deserializeBinaryFromReader);
      msg.setPlan(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.ro.tuc.ds2020.DownloadResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.ro.tuc.ds2020.DownloadResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.ro.tuc.ds2020.DownloadResponse} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.ro.tuc.ds2020.DownloadResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPlan();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.ro.tuc.ds2020.MedPlan.serializeBinaryToWriter
    );
  }
};


/**
 * optional MedPlan plan = 1;
 * @return {?proto.ro.tuc.ds2020.MedPlan}
 */
proto.ro.tuc.ds2020.DownloadResponse.prototype.getPlan = function() {
  return /** @type{?proto.ro.tuc.ds2020.MedPlan} */ (
    jspb.Message.getWrapperField(this, proto.ro.tuc.ds2020.MedPlan, 1));
};


/** @param {?proto.ro.tuc.ds2020.MedPlan|undefined} value */
proto.ro.tuc.ds2020.DownloadResponse.prototype.setPlan = function(value) {
  jspb.Message.setWrapperField(this, 1, value);
};


proto.ro.tuc.ds2020.DownloadResponse.prototype.clearPlan = function() {
  this.setPlan(undefined);
};


/**
 * Returns whether this field is set.
 * @return {!boolean}
 */
proto.ro.tuc.ds2020.DownloadResponse.prototype.hasPlan = function() {
  return jspb.Message.getField(this, 1) != null;
};



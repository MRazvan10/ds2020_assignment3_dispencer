/**
 * @fileoverview
 * @enhanceable
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

goog.provide('proto.ro.tuc.ds2020.MedicationTakenRequest');

goog.require('jspb.Message');
goog.require('jspb.BinaryReader');
goog.require('jspb.BinaryWriter');


/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.ro.tuc.ds2020.MedicationTakenRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.ro.tuc.ds2020.MedicationTakenRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  proto.ro.tuc.ds2020.MedicationTakenRequest.displayName = 'proto.ro.tuc.ds2020.MedicationTakenRequest';
}


if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto suitable for use in Soy templates.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     com.google.apps.jspb.JsClassTemplate.JS_RESERVED_WORDS.
 * @param {boolean=} opt_includeInstance Whether to include the JSPB instance
 *     for transitional soy proto support: http://goto/soy-param-migration
 * @return {!Object}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.ro.tuc.ds2020.MedicationTakenRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Whether to include the JSPB
 *     instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.ro.tuc.ds2020.MedicationTakenRequest} msg The msg instance to transform.
 * @return {!Object}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    patientid: jspb.Message.getFieldWithDefault(msg, 1, 0),
    date: jspb.Message.getFieldWithDefault(msg, 2, ""),
    medication: jspb.Message.getFieldWithDefault(msg, 3, ""),
    didtake: jspb.Message.getFieldWithDefault(msg, 4, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.ro.tuc.ds2020.MedicationTakenRequest}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.ro.tuc.ds2020.MedicationTakenRequest;
  return proto.ro.tuc.ds2020.MedicationTakenRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.ro.tuc.ds2020.MedicationTakenRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.ro.tuc.ds2020.MedicationTakenRequest}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPatientid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDate(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setMedication(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDidtake(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.ro.tuc.ds2020.MedicationTakenRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.ro.tuc.ds2020.MedicationTakenRequest} message
 * @param {!jspb.BinaryWriter} writer
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPatientid();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getDate();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getMedication();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDidtake();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
};


/**
 * optional int32 patientid = 1;
 * @return {number}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.getPatientid = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/** @param {number} value */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.setPatientid = function(value) {
  jspb.Message.setField(this, 1, value);
};


/**
 * optional string date = 2;
 * @return {string}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.getDate = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/** @param {string} value */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.setDate = function(value) {
  jspb.Message.setField(this, 2, value);
};


/**
 * optional string medication = 3;
 * @return {string}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.getMedication = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/** @param {string} value */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.setMedication = function(value) {
  jspb.Message.setField(this, 3, value);
};


/**
 * optional bool didtake = 4;
 * Note that Boolean fields may be set to 0/1 when serialized from a Java server.
 * You should avoid comparisons like {@code val === true/false} in those cases.
 * @return {boolean}
 */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.getDidtake = function() {
  return /** @type {boolean} */ (jspb.Message.getFieldWithDefault(this, 4, false));
};


/** @param {boolean} value */
proto.ro.tuc.ds2020.MedicationTakenRequest.prototype.setDidtake = function(value) {
  jspb.Message.setField(this, 4, value);
};



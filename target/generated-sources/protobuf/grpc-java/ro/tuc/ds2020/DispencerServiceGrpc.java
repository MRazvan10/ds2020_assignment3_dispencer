package ro.tuc.ds2020;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: DispencerService.proto")
public final class DispencerServiceGrpc {

  private DispencerServiceGrpc() {}

  public static final String SERVICE_NAME = "ro.tuc.ds2020.DispencerService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ro.tuc.ds2020.DownloadRequest,
      ro.tuc.ds2020.DownloadResponse> METHOD_DOWNLOAD =
      io.grpc.MethodDescriptor.<ro.tuc.ds2020.DownloadRequest, ro.tuc.ds2020.DownloadResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ro.tuc.ds2020.DispencerService", "download"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ro.tuc.ds2020.DownloadRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ro.tuc.ds2020.DownloadResponse.getDefaultInstance()))
          .build();
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<ro.tuc.ds2020.MedicationTakenRequest,
      ro.tuc.ds2020.MedicationTakenResponse> METHOD_TAKE_MEDICATION =
      io.grpc.MethodDescriptor.<ro.tuc.ds2020.MedicationTakenRequest, ro.tuc.ds2020.MedicationTakenResponse>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "ro.tuc.ds2020.DispencerService", "takeMedication"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ro.tuc.ds2020.MedicationTakenRequest.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              ro.tuc.ds2020.MedicationTakenResponse.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DispencerServiceStub newStub(io.grpc.Channel channel) {
    return new DispencerServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DispencerServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DispencerServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DispencerServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DispencerServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class DispencerServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void download(ro.tuc.ds2020.DownloadRequest request,
        io.grpc.stub.StreamObserver<ro.tuc.ds2020.DownloadResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_DOWNLOAD, responseObserver);
    }

    /**
     */
    public void takeMedication(ro.tuc.ds2020.MedicationTakenRequest request,
        io.grpc.stub.StreamObserver<ro.tuc.ds2020.MedicationTakenResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_TAKE_MEDICATION, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_DOWNLOAD,
            asyncUnaryCall(
              new MethodHandlers<
                ro.tuc.ds2020.DownloadRequest,
                ro.tuc.ds2020.DownloadResponse>(
                  this, METHODID_DOWNLOAD)))
          .addMethod(
            METHOD_TAKE_MEDICATION,
            asyncUnaryCall(
              new MethodHandlers<
                ro.tuc.ds2020.MedicationTakenRequest,
                ro.tuc.ds2020.MedicationTakenResponse>(
                  this, METHODID_TAKE_MEDICATION)))
          .build();
    }
  }

  /**
   */
  public static final class DispencerServiceStub extends io.grpc.stub.AbstractStub<DispencerServiceStub> {
    private DispencerServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DispencerServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DispencerServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DispencerServiceStub(channel, callOptions);
    }

    /**
     */
    public void download(ro.tuc.ds2020.DownloadRequest request,
        io.grpc.stub.StreamObserver<ro.tuc.ds2020.DownloadResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_DOWNLOAD, getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void takeMedication(ro.tuc.ds2020.MedicationTakenRequest request,
        io.grpc.stub.StreamObserver<ro.tuc.ds2020.MedicationTakenResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_TAKE_MEDICATION, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DispencerServiceBlockingStub extends io.grpc.stub.AbstractStub<DispencerServiceBlockingStub> {
    private DispencerServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DispencerServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DispencerServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DispencerServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ro.tuc.ds2020.DownloadResponse download(ro.tuc.ds2020.DownloadRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_DOWNLOAD, getCallOptions(), request);
    }

    /**
     */
    public ro.tuc.ds2020.MedicationTakenResponse takeMedication(ro.tuc.ds2020.MedicationTakenRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_TAKE_MEDICATION, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DispencerServiceFutureStub extends io.grpc.stub.AbstractStub<DispencerServiceFutureStub> {
    private DispencerServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DispencerServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DispencerServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DispencerServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.tuc.ds2020.DownloadResponse> download(
        ro.tuc.ds2020.DownloadRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_DOWNLOAD, getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ro.tuc.ds2020.MedicationTakenResponse> takeMedication(
        ro.tuc.ds2020.MedicationTakenRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_TAKE_MEDICATION, getCallOptions()), request);
    }
  }

  private static final int METHODID_DOWNLOAD = 0;
  private static final int METHODID_TAKE_MEDICATION = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DispencerServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DispencerServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DOWNLOAD:
          serviceImpl.download((ro.tuc.ds2020.DownloadRequest) request,
              (io.grpc.stub.StreamObserver<ro.tuc.ds2020.DownloadResponse>) responseObserver);
          break;
        case METHODID_TAKE_MEDICATION:
          serviceImpl.takeMedication((ro.tuc.ds2020.MedicationTakenRequest) request,
              (io.grpc.stub.StreamObserver<ro.tuc.ds2020.MedicationTakenResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class DispencerServiceDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ro.tuc.ds2020.DispencerServiceOuterClass.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DispencerServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DispencerServiceDescriptorSupplier())
              .addMethod(METHOD_DOWNLOAD)
              .addMethod(METHOD_TAKE_MEDICATION)
              .build();
        }
      }
    }
    return result;
  }
}

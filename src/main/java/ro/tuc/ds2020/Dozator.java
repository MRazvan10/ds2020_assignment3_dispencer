package ro.tuc.ds2020;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Dozator {

    private final static int PATIENT_ID = 2;
    private JButton[] butoane = new JButton[7];
    private static JPanel panel = new JPanel(new GridLayout(5,2));
    private JLabel timeLabel = new JLabel(new Date(System.currentTimeMillis()).toString());
    MyThread myThread;

    public Dozator() {
        panel.add(timeLabel);
        panel.setBackground(Color.BLUE);
        for(int i=1; i<=3; i++) {
            butoane[i] = new JButton("Pastila");
            butoane[i].setBackground(Color.WHITE);
            panel.add(butoane[i]);
            butoane[i].setEnabled(false);
        }
        myThread = new MyThread();
        myThread.start();
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Pastile");
        frame.setSize(2000, 2200);
        frame.setContentPane(new Dozator().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setBackground(Color.BLUE);
        frame.setVisible(true);
    }

    class MyThread extends Thread {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar calendar = Calendar.getInstance();

        int[] intervale = new int[7];
        int butoaneCount= 0;

        DownloadResponse downloadResponse = null;

        public MyThread() {
            calendar.setTime(new Date(System.currentTimeMillis()));
            download();
            for(int i=1; i<=3; i++) {
                int finalI = i;
                butoane[i].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        takeMedication(butoane[finalI].getText(), true, calendar.getTime().toString());
                        butoane[finalI].setText("Ati luat deja");
                        butoane[finalI].setEnabled(false);
                    }
                });
            }
        }

        private void download() {
            final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:9005")
                    .usePlaintext(true)
                    .build();

            DispencerServiceGrpc.DispencerServiceBlockingStub stub = DispencerServiceGrpc.newBlockingStub(channel);

            DownloadRequest downloadRequest = DownloadRequest.newBuilder()
                    .setPatientid(PATIENT_ID)
                    .setDate(calendar.get(Calendar.DAY_OF_MONTH) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.YEAR))
                    .build();

            downloadResponse = stub.download(downloadRequest);
            System.out.println(downloadResponse);

            Scanner s = new Scanner(downloadResponse.getPlan().getIntakeintervals());
            int nr = 0;
            while(s.hasNextInt()) {
                intervale[++nr] = s.nextInt();
            }
            butoaneCount = nr;
            System.out.println(nr);

            channel.shutdown();
        }

        private void takeMedication(String medication, boolean didTake, String date) {
            final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:9005")
                    .usePlaintext(true)
                    .build();
            DispencerServiceGrpc.DispencerServiceBlockingStub stub = DispencerServiceGrpc.newBlockingStub(channel);

            MedicationTakenRequest medicationTakenRequest = MedicationTakenRequest.newBuilder()
                    .setPatientid(PATIENT_ID)
                    .setMedication(medication)
                    .setDate(date)
                    .setDidtake(didTake)
                    .build();

            MedicationTakenResponse medicationTakenResponse = stub.takeMedication(medicationTakenRequest);
            System.out.println( medicationTakenResponse);
            channel.shutdown();
        }

        public void run() {
            while(true) {
                timeLabel.setText(dateFormat.format(calendar.getTime()));
                calendar.add(Calendar.HOUR, 1);

                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                if(hour == 0) {
                    download();
                }

                for(int i = 1; i <= butoaneCount; i++) {
                    if(butoane[i].isEnabled()) {
                        takeMedication(butoane[i].getText(), false, calendar.getTime().toString());
                    }
                }

                for(int i = 1; i <= butoaneCount; i++) {
                    if(hour % intervale[i] == 0) {
                        butoane[i].setText(downloadResponse.getPlan().getMedication(i-1));
                        butoane[i].setEnabled(true);
                    } else {
                        butoane[i].setEnabled(false);
                        butoane[i].setText("Asteapta");
                    }

                }

                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
